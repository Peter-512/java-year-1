package m1;

import java.util.Scanner;

public class Quiz {
    public static void main(String[] args) {
        int num;
        int result;
        int guess;
        String correctOrWrong;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Which multiplication table would you like to practice? ");
        num = keyboard.nextInt();
        for (int i = 1; i <= 10; i++) {
            result = i * num;
            System.out.print(i + " x " + num + " = ");
            guess = keyboard.nextInt();
            correctOrWrong = (result == guess) ? "Correct!" : "Wrong!";
            System.out.println(correctOrWrong);
        }
    }
}
