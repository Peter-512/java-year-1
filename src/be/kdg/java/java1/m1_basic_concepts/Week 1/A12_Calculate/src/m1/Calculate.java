package m1;

import java.util.Scanner;

public class Calculate {
    public static void main(String[] args) {
        int num1;
        int num2;
        int choice;
        String[] operations = {"add", "subtract", "multiply", "divide", "exponentiation", "modulo", "square"};
        Scanner keyboard = new Scanner(System.in);

        //reading in the numbers
        System.out.print("Enter a number: ");
        num1 = keyboard.nextInt();
        System.out.print("Enter another number: ");
        num2 = keyboard.nextInt();

        System.out.println("Choose an operation: ");

        //Choices of operations
        for (int i = 0; i < operations.length; i++) {
            System.out.println(i + 1 + ". " + operations[i]);
        }

        choice = keyboard.nextInt();

        //In case of wrong input (out of range of operations)
        while (choice < 1 || choice > operations.length) {
            System.out.print("Wrong input! Choose an operation (1-" + operations.length + "): ");
            choice = keyboard.nextInt();
        }

        System.out.println("Your choice: " + choice + " (" + operations[choice - 1] + ")");

        //Switch
        switch (choice) {
            case 1 -> // add
                    System.out.println(num1 + " + " + num2 + " = " + (num1 + num2));
            case 2 -> // subtract
                    System.out.println(num1 + " - " + num2 + " = " + (num1 - num2));
            case 3 -> // multiply
                    System.out.println(num1 + " * " + num2 + " = " + (num1 * num2));
            case 4 -> { // divide
                if (num2 == 0) {
                    System.out.println("Oops... Division by 0 ain't possible.");
                    System.out.print("Choose a different divisor: ");
                    num2 = keyboard.nextInt();
                }
                System.out.println(num1 + " / " + num2 + " = " + ((double) num1 / (double) num2));
            }
            case 5 -> { // exponentiation
                int result = num1;
                if (num2 == 0) result = 1;
                else {
                    for (int i = 1; i < num2; i++) {
                        result *= num1;
                    }
                }
                System.out.println(num1 + " to the power of " + num2 + " = " + result);
            }
            case 6 -> // modulo
                    System.out.println(num1 + " % " + num2 + " = " + (num1 % num2));
            default -> {
            }
        }
    }
}