package m1;

import java.util.Scanner;

public class HigherLower {
    public static void main(String[] args) {
        int secret = 49;
        int guess = 0;
        int attempts = 0;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter a number: ");
        while (guess != secret) {
            attempts++;
            guess = keyboard.nextInt();
            if (guess < secret) {
                System.out.println("Higher!");
                System.out.print("Try again: ");
            } else if (guess > secret) {
                System.out.println("Lower!");
                System.out.print("Try again: ");
            }
        }
        System.out.println("Congrats, your guess is correct!");
        System.out.println("You took " + attempts + " attempts.");
    }
}
