package m1;

import java.util.Scanner;

public class BMIv2 {
    public static void main(String[] args) {
        double bmi;
        double weight;
        double height;
        String bmiClass = null;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Dear patient, this programma will calculate your BMI.");

        System.out.println("Enter your weight in kilograms: ");
        weight = keyboard.nextDouble();
        System.out.println("Enter your height in meters: ");

        height = keyboard.nextDouble();
        bmi = weight / (height * height);

        System.out.println("Your BMI is: " + bmi);

        if (bmi < 18) bmiClass = "underweight";
        else if (bmi >= 18 && bmi < 25) bmiClass = "healthy weight";
        else if (bmi >= 25 && bmi < 30) bmiClass = "overweight";
        else if (bmi >= 30) bmiClass = "obese";

        System.out.println("Your bmi class is " + bmiClass);
    }
}
