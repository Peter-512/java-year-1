package m1;

import java.util.Scanner;

public class BMIv1 {
    public static void main(String[] args) {
        double bmi;
        double weight;
        double height;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("Dear patient, this programma will calculate your BMI.");
        System.out.println("Enter your weight in kilograms: ");
        weight = keyboard.nextDouble();
        System.out.println("Enter your height in meters: ");
        height = keyboard.nextDouble();
        bmi = weight / (height * height);
        System.out.println("Your BMI is: " + bmi);

    }
}
