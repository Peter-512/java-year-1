package be.kdg.java.java1.m6.assignments;

/**
 * - Create an array of 20 integers and fill it with multiples of 7 (7, 14, 21, 28, ...). Use a loop to do this. <br>
 * - Print the contents of this array using a for-each loop. <br>
 * - Print the contents of this array in reversed order using a regular for-loop. <br>
 */

public class a03ForEach {
    public static void main(String[] args) {
        int[] multiplesOf7 = new int[20];

        for (int i = 0; i < multiplesOf7.length; i++) {
            int num = 7 * (i + 1);
            multiplesOf7[i] = num;
        }

        for (int num : multiplesOf7) {
            System.out.printf("%d ", num);
        }

        System.out.println();

        for (int i = multiplesOf7.length - 1; i >= 0 ; i--) {
            System.out.printf("%d ", multiplesOf7[i]);
        }
    }
}
