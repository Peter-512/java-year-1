package be.kdg.java.java1.m6.assignments;

/**
 * Use a for-loop and the length property to print all numbers on a single line, separated by a space.
 * Change the value of the second number from 6 to 13.
 * Print the array again to check if the number was modified.
 */

public class a02Lottery {
    public static void main(String[] args) {
        int[] lotteryNumbers = {3, 6, 17, 31, 32, 43};

        for (int number : lotteryNumbers) {
            System.out.printf("%d ", number);
        }

        lotteryNumbers[1] = 13;
        System.out.println();

        for (int number : lotteryNumbers) {
            System.out.printf("%d ", number);
        }
    }
}
