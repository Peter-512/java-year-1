package be.kdg.java.java1.m6.assignments.a10playingcards;

import be.kdg.java.java1.m3_flow_control.assignments.HelpMe;

import java.util.Random;

/**
 * - Create a Card class <p>
 * - Create an array of 52 Card objects. 
 * - Use two nested for loops (one iterating over suits, the other iterating over ranks) to initialize the elements of this array with each card of a 52-card deck.<br>
 * - Ask the user how many cards the program should pick. This should be a number from 1 to 5 (both inclusive). If the user enters an invalid number, then display an appropriate error message. Name this variable cardCount.<br>
 * - Create an array of Card objects called selectedCards with a length equal to cardCount.<br>
 * - Fill the selectedCards array with random cards, taken from the 52-card deck.<br>
 * - Extra: make sure that no card is taken twice.<br>
 * - Finally, print the selected cards.<br>
 */

public class PlayingCards {
    public static void main(String[] args) {
        String[] suits = {
                "clubs", "diamonds", "spades", "hearts"
        };
        String[] ranks = {
                "ace", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten",
                "jack", "queen", "king"
        };

        final int DECK_SIZE = 52;
        Random rand = new Random();
        Card[] deck = new Card[DECK_SIZE];
        int[] pickedCards = new int[DECK_SIZE];
        int cardIndex = 0;
        int cardCount;

        // initializing the deck
        for (String suit : suits) {
            for (String rank : ranks) {
                deck[cardIndex] = new Card(rank, suit);
                cardIndex++;
            }
        }

        cardCount = HelpMe.readNumberBetweenBounds(1, 5, "number of cards");
        Card[] selectedCards = new Card[cardCount];

        for (int i = 0; i < selectedCards.length; i++) {
            int cardNum = rand.nextInt(DECK_SIZE);
            for (int j = 0; j < pickedCards.length; j++) {
                if (pickedCards[j] == cardNum) {
                    j = 0;
                    cardNum = rand.nextInt(DECK_SIZE);
                }
            }
            pickedCards[i] = cardNum;
            selectedCards[i] = deck[cardNum];
        }

        for (Card card : selectedCards) {
            System.out.printf("Suit: %s     Rank: %s %n", card.getSuit(), card.getRank());
        }
    }
}
