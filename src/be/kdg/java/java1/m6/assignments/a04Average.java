package be.kdg.java.java1.m6.assignments;

/**
 * Write a program that calculates the average of an array of numbers.
 */

public class a04Average {
    public static void main(String[] args) {
        int[] numbers = {
                12, 17, 14, 18, 13, 13, 14, 17, 17, 16,
                10, 18, 13, 18, 12, 12, 10, 17, 10, 15,
                10, 11, 16, 12, 16, 11, 8, 10, 16, 14,
                17, 7, 11, 10, 15, 10, 14, 8, 9, 14
        };
        int sum = 0;

        for (int num : numbers) {
            sum += num;
        }

        System.out.printf("The average is %.3f", (float) sum / numbers.length);
    }
}
