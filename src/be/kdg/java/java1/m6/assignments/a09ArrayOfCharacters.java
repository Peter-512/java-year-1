package be.kdg.java.java1.m6.assignments;

/**
 * Create an array of characters called letters that contains each character from the String object word.<br>
 * Don't do this manually, instead call a method on word to create the array!<p>
 *
 * Create a loop to print the array (character by character) like this:<br>
 * J a v a S c r i p t
 */

public class a09ArrayOfCharacters {
    public static void main(String[] args) {
        String word = "JavaScript";

        String[] letters = word.split("");

        for (String letter : letters) {
            System.out.printf("%s ", letter);
        }
    }
}
