package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * Calculate the growth of capital over the years at a given interest rate.
 *<p>
 * Ask the user to enter these data:
 *<p>
 * starting capital (double)
 * interest rate (double)
 * number of years (int)
 * Calculate the capital after the given number of years. Cast the capital to long before printing.
 * <p>
 * Extend the program by adding the number of years it will take to double the amount at this interest rate.
 */

public class Interest {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        double startCap;
        double intRate;
        int duration;
        double endCap;
        int year;
        double doubledCap;

        System.out.print("Enter the starting capital in €: ");
        startCap = keyboard.nextDouble();
        System.out.print("Enter the interest rate: ");
        intRate = keyboard.nextDouble();
        System.out.print("Enter the number of years: ");
        duration = keyboard.nextInt();
        keyboard.close();

        // calculation for set duration
        endCap = startCap;
        for (year = 0; year < duration; year++) {
            endCap *= (1 + intRate / 100);
        }

        // calculation when it reaches 2x
        doubledCap = startCap * 2;
        for (year = 0; startCap < doubledCap; year++) {
            startCap *= (1 + intRate / 100);
        }

        System.out.printf("The capital will amount to €%.2f", endCap);
        System.out.printf("\nIt takes %d years to double the money.", year);
    }
}
