package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * requests the user to enter his age
 * displays the age category of the user:
 * Baby < 2
 * Child 2-12
 * Teenager 13-17
 * Adult 18+
 */

public class Age {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int age;
        String ageGroup;

        System.out.print("Please enter your age: ");
        age = keyboard.nextInt();

        if (age < 0) {
            System.out.println("You aren't alive yet.");
            keyboard.close();
            return;
        }
        else if (age < 2) ageGroup = "baby";
        else if (age < 13) ageGroup = "child";
        else if (age < 18) ageGroup = "teenager";
        else if (age > 125) {
            System.out.println("You are most likely dead. Unless you can read this. In that case, congrats!");
            keyboard.close();
            return;
        }
        else ageGroup = "adult";

        System.out.printf("You are a%s %s", ageGroup.charAt(0) == 97 ? "n" : "", ageGroup);

        keyboard.close();
    }
}
