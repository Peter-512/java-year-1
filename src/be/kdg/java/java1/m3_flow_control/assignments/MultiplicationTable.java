package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Ask the user for a number between 1 and 30. Test the number and ask again if it is not correct. <br>
 * Print the multiplication table from 1 up to the given number
 */

public class MultiplicationTable {
    public static void main(String[] args) {
        final int MIN = 1;
        final int MAX = 30;

        System.out.print("Enter a number between 1 and 30: ");
        int num = HelpMe.readNumberBetweenBounds(MIN, MAX, "number");

        for (int row = 1; row <= num; row++) {
            System.out.print("|");
            for (int col = row; col <= num * row; col += row) {
                System.out.printf("%3d|", col);
            }
            System.out.println();
        }
    }
}
