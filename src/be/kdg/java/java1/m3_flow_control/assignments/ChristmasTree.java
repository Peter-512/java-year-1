package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Write a program to draw a Christmas tree using * and space characters.
 *<p>
 * Ask the user for the width of the tree, this should be an uneven number between 13 and 29 <br>
 * If the numbered entered is not within these boundaries, take the minimum width<br>
 * If the number is not uneven make the width one smaller
 */

public class ChristmasTree {
    public static void main(String[] args) {
        final int MIN = 13;
        final int MAX = 29;
        final int TRUNK_HEIGHT = 4;
        final String TRUNK_ROW = "***";
        final char TREE_PART = '*';
        final char WS = ' ';
        String format;
        int maxWidth;
        int whiteSpace;
        int width = 1;

        System.out.printf("Enter the with of the Christmas Tree (uneven number between %d and %d: ", MIN, MAX);
        do {
            maxWidth = HelpMe.readNumberBetweenBounds(MIN, MAX, "width");
            if (maxWidth % 2 != 1) System.out.print("Width needs to be uneven");
        } while (maxWidth % 2 != 1);

        whiteSpace = (maxWidth - 1) / 2;

        // render top of the tree
        while (width <= maxWidth) {
            format = "%" + whiteSpace + "c";
            if (whiteSpace > 0) System.out.printf(format, WS);
            for (int i = 0; i < width; i++) {
                System.out.print(TREE_PART);
            }
            System.out.println();
            width += 2;
            whiteSpace--;
        }
        // render trunk
        format = "%" + (maxWidth - 3) / 2 + "c";
        for (int i = 0; i < TRUNK_HEIGHT; i++) {
            System.out.printf(format, WS);
            System.out.print(TRUNK_ROW);
            System.out.println();
        }
    }
}
