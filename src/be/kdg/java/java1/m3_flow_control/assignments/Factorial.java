package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Show the factorials from 1 to 20
 */

public class Factorial {
    public static void main(String[] args) {
        // solution using recursion
        for (int number = 1; number <= 20; number++) {
            System.out.println(number + "! = " + calculateFactorial(number));
        }

        // solution using a loop and additional variable
        for (int number = 1; number <= 20; number++) {
            number *= number;
            System.out.println(number + "! = " + number);
        }
    }

    public static long calculateFactorial(int num) {
        if (num < 2) return num;
        return num * calculateFactorial(num - 1);
    }
}
