package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * Ask a user for a number.
 *
 * Show all multiples for that number below a maximum value.
 *
 * In your program initialise a constant MAX to 100.
 */

public class Multiples {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final int MAX = 100;
        int multiples;

        System.out.print("Which number would you like to see the multiples of? ");
        multiples = keyboard.nextInt();
        keyboard.close();
        for (int num = multiples; num < MAX; num += multiples) {
            System.out.println(num);
        }
    }
}
