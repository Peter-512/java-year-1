package be.kdg.java.java1.m3_flow_control.assignments;

import java.util.Scanner;

/**
 * Helper class containing helper functions:
 * <p>
 * readNumberBetweenBounds
 */

public class HelpMe {

    // TODO: implement static reusable keyboard or wrapper class
    // https://stackoverflow.com/questions/27286690/in-java-is-it-possible-to-re-open-system-in-after-closing-it/28929047#28929047

    /**
     * Reads in a number and checks if it's within bounds and will keep asking until input is valid.
     */
    public static int readNumberBetweenBounds(final int MIN, final int MAX, String inputName) {
        Scanner keyboard = new Scanner(System.in);
        System.out.printf("Enter a%s %s between %d and %d: ", isVowel(inputName.charAt(0)) ? "n" : "", inputName, MIN, MAX);
        int num = keyboard.nextInt();
        while (num < MIN || num > MAX) {
            System.out.printf("Invalid %s\n", inputName);
            System.out.print("Try again: ");
            num = keyboard.nextInt();
        }
//        keyboard.close(); // don't close the keyboard to be able to read in more than one number
                            // apparently scanner.close() closes the System.in Stream
        return num;
    }
    public static boolean isVowel(char c) {
        return "AEIOUaeiou".indexOf(c) != -1;
    }
    public static void printFuck() {
        System.out.println("FUCK");
    }
    public static void SHREK() {
        System.out.println("⢀⡴⠑⡄⠀⠀⠀⠀⠀⠀⠀⣀⣀⣤⣤⣤⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠸⡇⠀⠿⡀⠀⠀⠀⣀⡴⢿⣿⣿⣿⣿⣿⣿⣿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠑⢄⣠⠾⠁⣀⣄⡈⠙⣿⣿⣿⣿⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⢀⡀⠁⠀⠀⠈⠙⠛⠂⠈⣿⣿⣿⣿⣿⠿⡿⢿⣆⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⢀⡾⣁⣀⠀⠴⠂⠙⣗⡀⠀⢻⣿⣿⠭⢤⣴⣦⣤⣹⠀⠀⠀⢀⢴⣶⣆ \n" +
                "⠀⠀⢀⣾⣿⣿⣿⣷⣮⣽⣾⣿⣥⣴⣿⣿⡿⢂⠔⢚⡿⢿⣿⣦⣴⣾⠁⠸⣼⡿ \n" +
                "⠀⢀⡞⠁⠙⠻⠿⠟⠉⠀⠛⢹⣿⣿⣿⣿⣿⣌⢤⣼⣿⣾⣿⡟⠉⠀⠀⠀⠀⠀ \n" +
                "⠀⣾⣷⣶⠇⠀⠀⣤⣄⣀⡀⠈⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠉⠈⠉⠀⠀⢦⡈⢻⣿⣿⣿⣶⣶⣶⣶⣤⣽⡹⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⠀⠉⠲⣽⡻⢿⣿⣿⣿⣿⣿⣿⣷⣜⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣷⣶⣮⣭⣽⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⣀⣀⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀ \n" +
                "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠛⠻⠿⠿⠿⠿⠛⠉");
    }
    public static void PIKACHU() {
        System.out.println("⢀⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⣠⣤⣶⣶\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⢰⣿⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣀⣀⣾⣿⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⡏⠉⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⠀⠀⠀⠈⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⠉⠁⠀⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣧⡀⠀⠀⠀⠀⠙⠿⠿⠿⠻⠿⠿⠟⠿⠛⠉⠀⠀⠀⠀⠀⣸⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣷⣄⠀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠠⣴⣿⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⡟⠀⠀⢰⣹⡆⠀⠀⠀⠀⠀⠀⣭⣷⠀⠀⠀⠸⣿⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠈⠉⠀⠀⠤⠄⠀⠀⠀⠉⠁⠀⠀⠀⠀⢿⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⢾⣿⣷⠀⠀⠀⠀⡠⠤⢄⠀⠀⠀⠠⣿⣿⣷⠀⢸⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⡀⠉⠀⠀⠀⠀⠀⢄⠀⢀⠀⠀⠀⠀⠉⠉⠁⠀⠀⣿⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠈⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣿⣿\n" +
                "⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿");
    }
    public static void BONK() {
        String bonk = "⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⡀⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣔⢧⣓⢖⡮⣎⢮⢣⡫⡂⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⢠⡺⣧⣳⢵⡳⡝⡎⣎⢇⢏⢎⢲⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⡠⡄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⡀⡧⣫⡳⡳⡓⡝⢚⢻⢽⢺⢼⢼⢼⡁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠠⣀⢀⠄⠄⣠⢪⢣⠊⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⠄⢀⢠⡪⣞⣝⣞⢮⡳⡱⢌⢂⠢⡑⡸⡸⡹⣝⡾⣄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣀⡱⡀⠈⠄⡀⡮⡪⠊⠄⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⠄⢠⢞⣜⢮⢮⢺⢜⢷⢽⢮⢇⢇⢕⢵⡧⣇⢇⢗⡿⡻⠄⠄⠄⠄⠄⠄⢠⠄⢄⠈⠂⠄⢀⢖⢝⠜⠄⠄⡀⠄⠄⠄⠄⠄⠄⠄⠄\n" +
                "⠄⠄⡠⢯⢯⡺⡧⣳⡱⢍⠇⡏⢎⠫⡢⢊⠜⢜⢕⠝⠁⠄⠄⠄⠄⠄⠄⣀⢄⠈⠢⠜⠄⢀⢔⢇⡯⣢⢆⡶⡤⣄⡀⡐⠄⠁⠄⠐⠄⠄\n" +
                "⠄⢠⣝⢵⡫⡿⣝⢮⡪⡣⡓⠜⢔⢑⠌⢔⠨⢂⠢⠡⠄⠄⠄⠄⠄⠄⠄⠙⢆⠅⠄⠄⡆⡧⣫⡺⡼⡪⢷⢽⢯⣗⢯⡆⡄⠄⠄⠄⠄⠄\n" +
                "⠄⣞⢾⢵⡫⡯⣗⣝⢮⢢⠪⡘⡐⢅⢊⠔⡨⠢⡑⡅⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⢮⣺⢽⢳⡹⡰⡨⠪⡽⣕⢯⡳⣝⡦⡀⠄⠄⡀⠄\n" +
                "⠄⣞⢽⣝⢮⢯⣺⢮⣳⡳⡵⡸⡨⡢⣑⢑⢌⢎⠪⡢⠄⠄⠄⠄⠄⠄⠄⠄⢠⠪⣳⣟⢮⢳⢕⣽⢪⢘⢜⢜⢮⡳⡽⣕⡯⣖⠄⠄⠄⠄\n" +
                "⠄⠘⣝⢮⢯⣳⣳⣻⣺⡪⣳⢱⢱⢸⢰⢵⡕⡔⡑⡌⡀⠄⠄⠄⠄⠄⢀⠦⠃⠁⢻⣽⢕⣗⡽⡳⡱⡐⢕⢕⡳⡽⣝⡮⣯⡺⣕⡀⠄⠄\n" +
                "⠄⠄⠈⠹⢳⣳⣻⣞⡧⣏⢮⢪⠪⡪⡪⡳⡽⣰⢨⢊⠪⠪⠢⡢⢄⢔⠍⠄⠄⠄⠈⠈⢑⢗⠝⢜⠰⡘⣜⢼⣝⣞⣗⢯⡳⣝⢮⢆⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠈⠘⠷⢟⢮⡳⡱⣑⢌⠪⠨⠪⠺⠸⡨⡢⢄⣀⢰⠍⠄⠄⠄⠄⠄⠄⡀⢘⢌⢎⠪⡪⡪⡮⣗⢗⣗⢽⢕⢯⢮⡳⣯⠄⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⠄⠙⡮⡣⡁⠄⠄⠄⠄⠄⠄⠈⠨⡢⡪⠢⠋⠄⠄⠄⠄⠄⠁⠄⠨⡢⡑⡅⢕⠕⣕⢳⢹⢸⢪⢳⡹⡮⡯⡷⡅⠄\n" +
                "⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢯⢎⢆⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠁⠄⠄⠄⠄⠈⠄⠄⠄⠈⠂⠑⠘⠐⠑⠘⠘⠘⠘⠘⠊⠙⠙⠙⠋⠃⠄";
        bonk = bonk.replace("⠄"," ");
        System.out.println(bonk);
    }
}
