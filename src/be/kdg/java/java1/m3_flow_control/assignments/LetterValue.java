package be.kdg.java.java1.m3_flow_control.assignments;

//Ask the user for a letter and show the scrabble value for that letter

import java.util.Scanner;

public class LetterValue {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String letter;
        int points;

        System.out.print("Enter a letter to find out its scrabble value: ");
        letter = keyboard.nextLine().toUpperCase();

        points = switch (letter) {
            case "A", "E", "I", "O", "U", "L", "N", "S", "T", "R" -> 1;
            case "D", "G" -> 2;
            case "B", "C", "M", "P" -> 3;
            case "F", "H", "V", "W", "Y" -> 4;
            case "K" -> 5;
            case "J", "X" -> 8;
            case "Q", "Z" -> 10;
            default -> 0;
        };
        if (points != 0) System.out.printf("The scrabble value of %s is %d", letter, points);
        else System.out.println("Invalid input");

        keyboard.close();
    }
}
