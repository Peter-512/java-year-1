package be.kdg.java.java1.m3_flow_control.assignments;

/**
 * Write a program that prints all hexadecimal digits:
 */

public class Hexadecimal {
    public static void main(String[] args) {
        for (int i = 0; i <= 0xF; i++) {
            System.out.printf("%X ", i);
        }
    }
}
