package be.kdg.java.java1.m4.assignments;

import java.util.Scanner;

/**
 * Write a program that does the following.

Declare a constant called "MAX" with the value 20.
Ask the user for a number between 1 and MAX (inclusive).
Check if the user entered a valid digit (Hint: Scanner's hasNextInt() can help).
Check if the user's number is within the given range.
Create a StringBuilder and add all numbers from 1 up to the given number to this StringBuilder.
Print the StringBuilder.
Remove all spaces from the StringBuilder.
Print the StringBuilder again.
 */

public class StringBuilderV2 {
    public static void main(String[] args) {
        final int MAX = 20;
        int num = 0;
        Scanner keyboard = new Scanner(System.in);
        System.out.printf("Enter the highest number (1..%d): ", MAX);

        boolean isNotInt;
        while ((isNotInt = !keyboard.hasNextInt()) || (num = keyboard.nextInt()) < 1 || num > 20) {
            if (isNotInt) {
                keyboard.next();
                System.out.println("You didn't enter a number!");
            } else {
                System.out.println("The number should be between 1 and 20!");
            }
            System.out.printf("Enter the highest number (1..%d): ", MAX);
        }

        StringBuilder numbers = new StringBuilder();
        for (int i = 1; i <= num; i++) {
            numbers.append(i).append(' ');
        }
        System.out.println(numbers);

        while (numbers.indexOf(" ") >= 0) {
            numbers.deleteCharAt(numbers.indexOf(" "));
        }
        System.out.println(numbers);
    }
}
