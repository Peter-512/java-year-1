package be.kdg.java.java1.m4.assignments;

import java.util.Scanner;

/**
 * Read a sentence entered by the user. Next, print the following:
 *<p>
 * The sentence in upper case<br>
 * The sentence in lower case<br>
 * The sentence where each 'a' is replaced by an 'o'<br>
 * The total length of the sentence<br>
 * The first character<br>
 * The last character<br>
 * The amount of times 'e' appears in the sentence<br>
 */

public class StringManipulation {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter a message: ");
        String message = keyboard.nextLine();

        System.out.printf("%s\n", message);
        System.out.printf("%s\n", message.toUpperCase());
        System.out.printf("%s\n", message.toLowerCase());
        System.out.printf("%s\n", message.replace('a', 'o'));
        System.out.printf("%s\n", message.length());
        System.out.printf("%s\n", message.charAt(0));
        System.out.printf("%s\n", message.charAt(message.length() - 1));
        System.out.printf("%s\n", message.chars().filter(letter -> letter == 'e').count());

        keyboard.close();
    }
}
