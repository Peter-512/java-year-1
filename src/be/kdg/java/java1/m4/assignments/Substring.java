package be.kdg.java.java1.m4.assignments;

/**
 * Write a program that counts how many times a substring appears within a String.

Let's find out how many times "ou" appears in the sentence "The more you learn, the less you know.".

The String documentation can help you.
 */

public class Substring {
    public static void main(String[] args) {

//        long start = System.nanoTime();

//        for (int idx = 0; idx < 1_000_000; idx++) {
            String string = "The more you learn, the less you know and the dumber you are.";
            String substr = "ou";
            int i = string.indexOf(substr);
            int len = substr.length();
            int count = 0;
            while (i >= 0) {
                i = string.indexOf(substr, i + len);
                count++;
            }
            System.out.printf("The substring \"%s\" appears %d time%s.%n", substr, count, count == 1 ? "" : "s");
//        }


//        long end = System.nanoTime();
//        System.out.println("Location: " + (end - start) * Math.pow(10, -9));
    }
}
