package be.kdg.java.java1.m4.assignments;

public class SubstringRecursive {

    public static void main(String[] args) {
//        long start = System.nanoTime();

//        for (int i = 0; i < 1_000_000; i++) {
            String string = "The more you learn, the less you know and the dumber you are.";
            String substr = "ou";
            int count = substringCount(string, substr);

            System.out.printf("The substring \"%s\" appears %d time%s.%n", substr, count, count == 1 ? "" : "s");
//        }


//        long end = System.nanoTime();
//        System.out.println("Location: " + (end - start) * Math.pow(10, -9));
    }
    public static int substringCount(StringBuilder string, String substring) {
        if (string.indexOf(substring) < 0) return 0;
        int idx = string.indexOf(substring);
        return 1 + substringCount(string.delete(0, idx + substring.length()), substring);
    }
    public static int substringCount(String string, String substring) {
        if (!string.contains(substring)) return 0;
        StringBuilder stringBuilder = new StringBuilder(string);
        int idx = stringBuilder.indexOf(substring);
        return 1 + substringCount(stringBuilder.delete(0, idx + substring.length()), substring);
    }
}
