package be.kdg.java.java1.m4.assignments;

import java.util.Scanner;

public class ComparingStrings {
    public static void main(String[] args) {
// PART 1
    // start of copied code
        String literal = "Harry";
        String newReference = "Harry";
        String newObject = new String("Harry");

        // Check if literal and newReference are the same.
        // Compare them using ==.
        System.out.printf("%b%n", literal == newReference);

        // Check if literal and newObject are the same.
        // Compare them using ==.
        System.out.printf("%b%n", literal == newObject);

        // Check if literal and newReference are the same.
        // Compare them using the 'equals' method of String.
        System.out.printf("%b%n", literal.equals(newReference));

        // Check if literal and newObject are the same.
        // Compare them using the 'equals' method of String.
        System.out.printf("%b%n", literal.equals(newObject));
    // end of copied code

// PART 2
        String stringOne;
        String stringTwo;

        // Read both strings using a Scanner.
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a String: ");
        stringOne = keyboard.nextLine();
        System.out.print("Enter another String: ");
        stringTwo = keyboard.nextLine();
        keyboard.close();

        // Trim both strings.
        stringOne.trim();
        stringTwo.trim();

        // Compare them and print them in alphabetical order.
        String first = stringOne.compareTo(stringTwo) < 0 ? stringOne : stringTwo;
        String second = stringOne.compareTo(stringTwo) < 0 ? stringTwo : stringOne;
        System.out.printf("%s %s%n", first, second);

        
// PART 3
        /*
          In the next program we want to check if three predefined strings are equal to each other, but without using
          "equals". Compare the first string to the second one, the first one to the third one and then the second one
          to the third one. Comparison should be done without taking letter casing into account.
         */
    // start of copied code
        String string1 = "java";
        String string2 = "Java";
        String string3 = "JAVA";

        // Complete this program
    // end of copied code
        System.out.printf("%s and %s are%s equal%n", string1, string2, string1.compareToIgnoreCase(string2) == 0 ? "" : " not");
        System.out.printf("%s and %s are%s equal%n", string1, string3, string1.compareToIgnoreCase(string3) == 0 ? "" : " not");
        System.out.printf("%s and %s are%s equal%n", string2, string3, string2.compareToIgnoreCase(string3) == 0 ? "" : " not");
    }
}
