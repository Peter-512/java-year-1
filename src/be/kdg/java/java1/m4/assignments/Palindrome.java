package be.kdg.java.java1.m4.assignments;

import java.util.Scanner;

/**
 * Write a program that can determine whether a word is a palindrome (a word which reads the same, backward as forward).
 * <p>
 * Use StringBuilder to accomplish this.
 */

public class Palindrome {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        System.out.print("Enter a word: ");
        String word = keyboard.nextLine();
        keyboard.close();

        System.out.printf("\"%s\" is%s a palindrome.%n", word, isPalindrome(word) ? "" : " not");
    }   
    public static boolean isPalindrome(String word) {
        word = word.toUpperCase();
        StringBuilder wordCopy = new StringBuilder(word);
        StringBuilder wordReverse = new StringBuilder(word).reverse();
        return wordCopy.compareTo(wordReverse) == 0;
    } 
}
