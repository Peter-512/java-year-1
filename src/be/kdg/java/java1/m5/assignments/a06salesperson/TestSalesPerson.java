package be.kdg.java.java1.m5.assignments.a06salesperson;

import java.util.Scanner;

public class TestSalesPerson {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name;

        name="Jan";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson one = new SalesPerson(name, scanner.nextDouble());

        name="Laetitia";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson two = new SalesPerson(name, scanner.nextDouble());

        name="Lotte";
        System.out.printf("Enter %s's revenue: ",name);
        SalesPerson three = new SalesPerson(name, scanner.nextDouble());

        SalesPerson topEarner;
        topEarner = one.hasMoreRevenueThan(two).hasMoreRevenueThan(three);
        System.out.printf("Our top earner is %s! %n", topEarner.getName());

        scanner.close();
    }
}
