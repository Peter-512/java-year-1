package be.kdg.java.java1.m5.assignments.a08perfectnumber;

public class PerfectNumber {
    String getPerfect(long number) {
        StringBuilder result = new StringBuilder(number + " --> ");
        long sum = 0;
        for (long i = 1; i <= number && sum <= number / 2; i++) {
            if (number % i == 0) {
                result.append(i).append(" ");
                sum += i;
            }
        }
        if (sum == number) {
            return result.toString();
        } else  {
            return null;
        }
    }
}
