package be.kdg.java.java1.m5.assignments.a03box;

public class Box {
    private String type;
    private double length;
    private double width;
    private double height;

    //CONSTRUCTORS
    public Box(String type, double length) {
        this(type, length, length, length);
    }

    public Box(String type, double length, double width, double height) {
        setType(type);
        setLength(length);
        setWidth(width);
        setHeight(height);
    }

    //SETTERS & GETTERS
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    //METHODS
    private double surface() {
        return width * length * 2 + width * height * 2 + length * height * 2;
    }
    private double volume() {
        return width * height * length;
    }
    private double tapeLength() {
        double len1 = Math.min(Math.min(getLength(), getHeight()), getWidth());
        double len2 = Math.max(Math.min(getLength(), getHeight()), getWidth());
        return 2 * len1 + 2 * len2;
    }
    @Override
    public String toString() {
        return String.format(
                "Type: %s %n" +
                "Length: %.1f %n" +
                "Width: %.1f %n" +
                "Height: %.1f %n" +
                "Surface: %.1f %n" +
                "Volume: %.1f %n" +
                "Minimum tape length: %.1f %n", getType(), getLength(), getWidth(), getHeight(), surface(), volume(), tapeLength());
    }
}
