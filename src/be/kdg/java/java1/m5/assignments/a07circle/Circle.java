package be.kdg.java.java1.m5.assignments.a07circle;

public class Circle {
    private int radius;
    private String color;

    // CONSTRUCTORS
    public Circle(int radius) {
        this(radius, "Black");
    }

    public Circle(int radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    // GETTERS & SETTERS
    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // METHODS
    public double circumference() {
        return 2 * radius * Math.PI;
    }

    public double surface() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double deltaCircumference(Circle other) {
        return Math.abs(circumference() - other.circumference());
    }

    public double deltaSurface(Circle other) {
        return Math.abs(surface() - other.surface());
    }

    public String toString() {
        return String.format("Colour: %s \n" +
                "Radius: %d \n" +
                "Circumference: %.2f \n" +
                "Surface: %.2f %n", getColor(), getRadius(), circumference(), surface());
    }
}
