package be.kdg.java.java1.m5.client;

import be.kdg.java.java1.m3_flow_control.assignments.HelpMe;
import be.kdg.java.java1.m5.graphics.Rectangle;

public class RectangleApp {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle();
        Rectangle rect2 = new Rectangle();
        Rectangle rect3 = new Rectangle(40, 65);
        rect1.displayInfo();

        rect2.setPosition(200, 3000);
        rect2.setSize(35, 45);
        rect1.grow(10);
        rect2.displayInfo();

        rect1.setHeight(25);
        rect1.setWidth(40);
        rect1.displayInfo();
        rect3.displayInfo();
        HelpMe.PIKACHU();
    }
}
