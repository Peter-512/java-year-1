package be.kdg.java.java1.m5.graphics;

public class Rectangle {
    final static int MIN_HEIGHT = 10;
    final static int MAX_HEIGHT = 10_000;
    final static int MIN_WIDTH = 10;
    final static int MAX_WIDTH = 10_000;
    final static int DEFAULT_WIDTH = 25;
    final static int DEFAULT_HEIGHT = 25;

    private int x;
    private int y;
    private int height;
    private int width;

    //CONSTRUCTORS
    public Rectangle() {
        setHeight(DEFAULT_HEIGHT);
        setWidth(DEFAULT_WIDTH);
    }
    public Rectangle(int height, int width) {
        setHeight(height);
        setWidth(width);
    }
    public Rectangle(int x, int y, int height, int width) {
        setX(x);
        setY(y);
        setHeight(height);
        setWidth(width);
    }
    public Rectangle(Rectangle rect) {
        setX(rect.x);
        setY(rect.y);
        setWidth(rect.width);
        setHeight(rect.height);
    }

    public void displayInfo() {
        System.out.printf("x: %d %n", this.x);
        System.out.printf("y: %d %n", this.y);
        System.out.printf("Height: %d %n", this.height);
        System.out.printf("Width: %d %n", this.width);
        System.out.printf("Area: %f %n", this.getArea());
        System.out.printf("Perimeter: %f %n", this.getPerimeter());
        System.out.println();
    }
    // HEIGHT
    public void setHeight(int height) {
        if (height < MAX_HEIGHT && height > MIN_HEIGHT) this.height = height;
        else System.out.println("Invalid height");
    }
    public int getHeight() {
        return height;
    }
    // WIDTH
    public void setWidth(int width) {
        if (width < MAX_WIDTH && width > MIN_WIDTH) this.width = width;
        else System.out.println("Invalid width");
    }
    public int getWidth() {
        return width;
    }
    // SIZE
    public void setSize(int width, int height) {
        if (height < MAX_HEIGHT && height > MIN_HEIGHT && width < MAX_WIDTH && width > MIN_WIDTH) {
            this.height = height;
            this.width = width;
        } else {
            System.out.println("Invalid size");
        }
    }
    // X
    public void setX(int x) {
        this.x = x;
    }
    public int getX() {
        return x;
    }
    // Y
    public void setY(int y) {
        this.y = y;
    }
    public int getY() {
        return y;
    }
    // SET POSITION
    public void setPosition(int x, int y) {
        this.y = y;
        this.x = x;
    }
    // GROW
    public void grow (int d) {
        grow(d, d);
    }
    public void grow(int dw, int dh) {
        if (width + dw < MAX_WIDTH && width + dw > MIN_WIDTH && height + dh < MAX_HEIGHT && height + dh > MIN_HEIGHT) {
            this.width += dw;
            this.height += dh;
        } else {
            System.out.println("Invalid resulting size");
        }
    }
    // AREA
    public double getArea() {
        return this.height * this.width;
    }
    // PERIMETER
    public double getPerimeter() {
        return this.height * 2 + this.width * 2;
    }
}
