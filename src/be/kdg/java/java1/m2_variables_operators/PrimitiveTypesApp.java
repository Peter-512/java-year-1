package be.kdg.java.java1.m2_variables_operators;

public class PrimitiveTypesApp {
    public static void main(String[] args) {
        byte counterByte = 127;
        short counterShort = 32_767;
        int counterInt = 2_147_483_647;
        long counterLong = 1_247_418_127_475L; // or cast as (long)

        float counterFloat = 35.5f; // or (float) 35.5
        double counterDouble = 1.0E-8;

        boolean bool = true;

        char character = 65; // ASCII for 'A'
        char secondChar = 'O';

        final int SPEED_OF_LIGHT = 300_000; // keyword final creates a constant that can't be changed anymore

        System.out.println(counterByte);
        System.out.println(character);
        System.out.println(counterShort);
        System.out.println(counterInt);
        System.out.println(counterLong);
        System.out.println(counterFloat);
        System.out.println(counterDouble);
        System.out.println(bool);
        System.out.println(secondChar);
        System.out.println(SPEED_OF_LIGHT);
    }
}
