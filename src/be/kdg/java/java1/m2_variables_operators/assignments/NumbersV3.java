package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program to divide a 13-digit number by an 8-digit number.
 *
 * - Create a constant MINIMUM_DIVIDEND with a value of 1000000000000 (12 zeroes = 1 trillion)
 * - Create a constant MINIMUM_DIVISOR with a value of 10000000 (10 million)
 * - Ask the user to enter a number of at least 13 digits (the dividend). Next, ask the user to enter a number of at least
 *   8 digits (the divisor). In both cases check if the number is large enough. If not, print an error message and abort the
 *   program.
 * - In case both numbers are accepted calculate the quotient and print it. Finally, print the quotient once again, but this
 *   time print it as a whole number (no decimal point and no fractional part)
 */

import java.util.Scanner;

public class NumbersV3 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final long MINIMUM_DIVIDEND = 1_000_000_000_000L;
        final long MINIMUM_DIVISOR = 10_000_000;
        long dividend;
        long divisor;

        System.out.print("Enter a number at least 13 digits long: ");
        dividend = keyboard.nextLong();
        if (dividend < MINIMUM_DIVIDEND) {
            System.out.println("This number is too small.");
            keyboard.close();
            return;
        }

        System.out.print("Enter another number at least 8 digits long: ");
        divisor = keyboard.nextLong();
        if (divisor < MINIMUM_DIVISOR) {
            System.out.println("This number is too small.");
            keyboard.close();
            return;
        }

        System.out.println("The quotient is " + (double) dividend / (double) divisor );
        System.out.println("Without the fractional part it's " + dividend / divisor);

        keyboard.close();
    }
}
