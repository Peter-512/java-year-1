package be.kdg.java.java1.m2_variables_operators.assignments;

/*
  Write a program in which you ask the user for four digits. Your program is then supposed to create a number out of these
  four digits. The first digit represents 1000s, the next one 100s, etc.

  Sample input:
  Enter four digits (0..9).
  The first digit: 9
  The second digit: 5
  The third digit: 7
  The fourth digit: 9
  The number is 9579

  Possible extras:

  - Validate the input
  - Tricky: make sure that the program repeats until the user enters -1 as his/her first digit
       (you can use a boolean variable to achieve this)
  - Hard: instead of an int, read the input as a string of text (using nextLine) and then convert the first character to an int.
 */

import java.util.Scanner;

public class DigitsV1 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int num = 0;
        long multiplier = 1;
        long result = 0;

        System.out.println("Create a number by entering one digit at a time.");

        while (num != -1) {
            if (multiplier == 1) System.out.print("Enter a digit (stop with -1): ");
            else System.out.print("Enter another digit (stop with -1): ");
            num = keyboard.nextInt();
            while (num < 0 || num > 9) {
                if (num == -1) break;
                System.out.println("Invalid input! Digit needs to be from 0 - 9.");
                System.out.print("Enter a valid digit: ");
                num = keyboard.nextInt();
            }
            if (num == -1) break;
            result += num * multiplier;
            multiplier *= 10;
        }
        System.out.println(result);
        keyboard.close();
    }
}
