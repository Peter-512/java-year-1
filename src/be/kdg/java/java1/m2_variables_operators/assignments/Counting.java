package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program that prints the numbers from 1 to 10 and from 10 to 1 next to each other. See the sample below.
 *
 * Use a constant MAX with the value 10.
 *
 * Use operators ++ and --.
 *
 * Challenge: try to write a while-loop with just a single line of code in it (a println statement).
 */

public class Counting {
    public static void main(String[] args) {
        final byte MAX = 10;
        byte incrementing = 1;
        byte decrementing = 10;

//        for (int i = 1; i <= MAX ; i++) {
//            System.out.printf("%2d - %-2d\n", i, 11-i);
//        }
        while (incrementing <= MAX) {
            System.out.printf("%2d - %-2d\n", incrementing++, decrementing--);
        }
    }
}
