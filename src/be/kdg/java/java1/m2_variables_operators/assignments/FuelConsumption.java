package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.Scanner;

/**
 * Write a program to calculate a car's fuel consumption.
 *
 * Ask for the following input:
 *
 * The car's mileage the last time its tank was filled.
 * The current mileage of the car.
 * The amount of fuel (in liters) that was used to fill the tank.
 */

public class FuelConsumption {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int prevMileage;
        int currMileage;
        double litersRefilled;
        int distance;
        double consumption;

        System.out.print("Enter the previous mileage: ");
        prevMileage = keyboard.nextInt();
        System.out.print("Enter the current mileage: ");
        currMileage = keyboard.nextInt();
        System.out.print("Enter the amount of liters refilled: ");
        litersRefilled = keyboard.nextDouble();

        distance = currMileage - prevMileage;
        consumption = litersRefilled / distance * 100;

        System.out.printf("Consumption for %dkm driven: %.2f liters/100km", distance, consumption);

        keyboard.close();
    }
}
