package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program that prints an ASCII table of all characters with a decimal code of 32 up to 127.
 * See the example below, each line of the output contains six characters.
 *
 * Extra: Also print all characters with a decimal code of 128 up to 255.
 */

public class ASCII_Table {
    public static void main(String[] args) {
        for (char c = 32; c < 256; c++) {
            System.out.printf("%c (%3d)",c, (int) c);
            if (c % 6 == 1) System.out.print("\n");
            else System.out.print(" \t");
        }
    }
}
