package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program that can multiply two six-digit numbers.
 *
 * Create a constant MINIMUM with a value of 100000.
 * Create a constant MAXIMUM with a value of 999999.
 * Ask the user for two six-digit numbers.
 * Check if both number do, in fact, have exactly six digits. Display an error message if this is not the case and abort
 * the program. (See the example below)
 * If both digits are accepted, then multiply them and extract the five final digits of the result. (Hint: you can use one
 * of the five most basic operators to do this)
 */

// Calculate the amount of digits of a positive number: int length = (int) (Math.log10(number) + 1);

import java.util.Scanner;

public class NumbersV2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final int MINIMUM = 100000;
        final int MAXIMUM = 999999;
        int num1;
        int num2;
        long product;
        long product5digits;

        System.out.print("Enter a 6-digit whole number: ");
        num1 = keyboard.nextInt();
        System.out.print("Enter another 6-digit whole number: ");
        num2 = keyboard.nextInt();

        if (num1 < MINIMUM || num2 < MINIMUM) {
            System.out.println("One of the numbers is too small.");
            keyboard.close();
            return;
        }
        if (num1 > MAXIMUM || num2 > MAXIMUM) {
            System.out.println("One of the numbers is too big.");
            keyboard.close();
            return;
        }
        product = (long) num1 * num2;
        System.out.println("The product is: " + product);
        product5digits = product % MINIMUM;
        System.out.printf("The final 5 digits are: %05d", product5digits);

        keyboard.close();
    }
}
