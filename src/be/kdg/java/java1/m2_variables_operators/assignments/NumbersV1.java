package be.kdg.java.java1.m2_variables_operators.assignments;

/**
 * Write a program in which you calculate the sum of two int variables, each of which contains the value of 2 billion (2,000,000,000).
 *
 * Print this sum.
 */

public class NumbersV1 {
    public static void main(String[] args) {

        //part 1
        int num1 = 2_000_000_000;
        int num2 = 2_000_000_000;
        long sum = (long) num1 + (long) num2;
        System.out.println(sum);

        //part 2
        long longNum1 = 10_000;
        long longNum2 = 10_000;
        int longSum = (int) longNum1 + (int) longNum2;
        System.out.println(longSum);

        //part 3
        int first = 8;
        int second = 5;
        System.out.println(first + second);
        System.out.println(first - second);
        System.out.println(first * second);
        System.out.println(first / second);
        System.out.println(first % second);

        // part 4
        int result;
        result = ++first; // first gets incremented, then assigned to result --> result = 9; first = 9
        System.out.println(result);
        result = first++; // first gets assigned to result, then gets incremented --> result = 9; first = 10
        System.out.println(result);
        result = --second; // second gets decremented, then assigned to result --> result = 4; second = 4
        System.out.println(result);
        result = second--; // second gets assigned to result, then gets decremented --> result = 4; second = 3
        System.out.println(result);
    }
}
