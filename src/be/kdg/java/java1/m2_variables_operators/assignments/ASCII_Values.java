package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.Scanner;

/**
 * Write a program that displays the decimal ASCII value of each individual character of a given string of text.
 */



public class ASCII_Values {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String sentence;
        
        System.out.print("Enter a string of text: ");
        sentence = keyboard.nextLine();

        for (int c = 0; c < sentence.length(); c++) {
            System.out.printf("%c has an ASCII value of %d\n", sentence.charAt(c), (int) sentence.charAt(c));
        }
        keyboard.close();
    }
}
