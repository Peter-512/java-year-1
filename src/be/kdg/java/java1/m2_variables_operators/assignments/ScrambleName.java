package be.kdg.java.java1.m2_variables_operators.assignments;

import java.util.ArrayList;
import java.util.Scanner;

/*
  Write a program to shuffle the letters of a person's name.

  Spaces should be removed.

  Hint: check the documentation of the class Random.
 */

public class ScrambleName {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        StringBuilder scrambled = new StringBuilder();
        ArrayList<Integer> usedIndexes = new ArrayList<Integer>();
        String name;
        int index;

        System.out.print("Enter your name: ");
        name = keyboard.nextLine();
        keyboard.close();

        name = name.replaceAll("\\s",""); // remove all whitespaces


        // adding letters to an ArrayList of Characters and shuffling them using Collections.shuffle
//        ArrayList<Character> letters = new ArrayList<>();
//        for (char letter : name.toCharArray()) {
//            letters.add(letter);
//        }
//
//        Collections.shuffle(letters);
//        for (char letter : letters) {
//            scrambled.append(letter);
//        }

        for (int i = 0; i < name.length(); i++) {
            index = (int) Math.floor(Math.random()*name.length()); // calculate random index
            while (usedIndexes.contains(index)) { // if index has already been used, go to next index
                index = (index + 1) % name.length(); // % operation to stay within bounds
            }
            usedIndexes.add(index);
            scrambled.append(name.charAt(index));
        }

        System.out.println(scrambled);

    }
}
