package be.kdg.java.java2.m7.assignments.a06product_constructor;

import java.util.Objects;

public class Book extends Product {
    private String title;
    private String author;

    public Book(String code, String description, double price, String title, String author) {
        super(code, description, price);
        this.title = title;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    @Override
    public double getVat() {
        return 0.06;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return Objects.equals(getTitle(), book.getTitle()) && Objects.equals(getAuthor(), book.getAuthor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTitle(), getAuthor());
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Book: %s by %s \n", getTitle(), getAuthor());
    }
}
