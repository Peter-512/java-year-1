package be.kdg.java.java2.m7.assignments.a06product_constructor;

import java.util.Objects;

public class Product {
    private String code;
    private String description;
    private double price;

    public Product(String code, String description, double price) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public double getPrice() {
        return price * (1 + getVat());
    }

    public String getDescription() {
        return description;
    }

    public double getVat() {
        return 0.21;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.getPrice(), getPrice()) == 0 &&
                Objects.equals(getCode(), product.getCode()) &&
                Objects.equals(getDescription(), product.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode(), getDescription(), getPrice());
    }

    @Override
    public String toString() {
        return String.format("Code: %-10s Description: %-25s Price: %.2f€ \n", getCode(), getDescription(), getPrice());
    }
}
