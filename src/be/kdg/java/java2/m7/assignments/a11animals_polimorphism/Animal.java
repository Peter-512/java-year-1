package be.kdg.java.java2.m7.assignments.a11animals_polimorphism;

public class Animal {
    private final String CHIP_NUMBER;
    private String name;
    private String breed;
    private String color;
    private String tagLine;

    public Animal(String name, String breed, String color, String CHIP_NUMBER) {
        this.name = name;
        this.breed = breed;
        this.color = color;
        this.CHIP_NUMBER = CHIP_NUMBER;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCHIP_NUMBER() {
        return CHIP_NUMBER;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    @Override
    public String toString() {
        return String.format("%s is a %s %s %s",name, color, breed, getCHIP_NUMBER() == null ? "" : "with chip " + getCHIP_NUMBER());
    }
}
