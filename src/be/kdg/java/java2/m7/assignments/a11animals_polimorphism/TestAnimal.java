package be.kdg.java.java2.m7.assignments.a11animals_polimorphism;

public class TestAnimal {
    public static void main(String[] args) {
        Animal[] garden = {new Dog("Leonardo", "Border Collie", "black", "5522"),
        new Rabbit("Donatello", "Angora", "gray", null, true),
        new Animal("Michelangelo", "Muppet", "brown", null)};

        for (Animal animal : garden) {
            System.out.println(animal);
        }
    }
}
