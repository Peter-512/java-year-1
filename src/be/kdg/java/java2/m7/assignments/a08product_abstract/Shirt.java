package be.kdg.java.java2.m7.assignments.a08product_abstract;

import java.util.Objects;

public class Shirt extends Product {
    private String size;
    private String gender;

    public Shirt(String code, String description, double price, String size, String gender) {
        super(code, description, price);
        this.size = size;
        this.gender = gender;
    }

    public String getSize() {
        return size;
    }

    public String getGender() {
        return gender;
    }

    public double getVat() {
        return 0.21;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Shirt shirt = (Shirt) o;
        return Objects.equals(getSize(), shirt.getSize()) && Objects.equals(getGender(), shirt.getGender());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSize(), getGender());
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Shirt: %s for %s \n", getSize(), getGender());
    }
}
