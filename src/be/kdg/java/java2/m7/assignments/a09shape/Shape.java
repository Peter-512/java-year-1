package be.kdg.java.java2.m7.assignments.a09shape;

public abstract class Shape {
    protected int x;
    protected int y;

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Shape() {
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    @Override
    public String toString() {
        return String.format("%s \t X: %d \t Y: %d %n",getClass().getSimpleName(), x, y);
    }
}
