package be.kdg.java.java2.m7.assignments.a09shape;

public class ShapeApp {
    public static void main(String[] args) {
        Circle c = new Circle(5);
        Rectangle r = new Rectangle(2,3, 4, 5);
        Square s = new Square(10);
        System.out.println(c);
        System.out.println(r);
        System.out.print(s);
    }
}
