package be.kdg.java.java2.m7.assignments.a09shape;

public class Circle extends Shape {
    private int radius;

    public Circle() {
    }

    public Circle(int x, int y) {
        super(x, y);
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * Math.PI;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Perimeter: %.2f \t Area: %.2f", getPerimeter(), getArea());
    }
}
