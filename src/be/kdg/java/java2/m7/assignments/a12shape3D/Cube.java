package be.kdg.java.java2.m7.assignments.a12shape3D;

public class Cube extends Shape3D {
    private double edge = 1.0;

    public Cube() {}

    public Cube(String colour, double edge) {
        super(colour);
        this.edge = edge;
    }

    public Cube(double edge) {
        this.edge = edge;
    }

    @Override
    public double surface() {
        return 6 * edge * edge;
    }

    @Override
    public double volume() {
        return Math.pow(edge, 3);
    }
}
