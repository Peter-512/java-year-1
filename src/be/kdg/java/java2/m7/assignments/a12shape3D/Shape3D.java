package be.kdg.java.java2.m7.assignments.a12shape3D;

public abstract class Shape3D {
    private String colour="black";

    public Shape3D(String colour) {
        this.colour = colour;
    }

    /**
     * Default constructor setting colour black.
     */
    public Shape3D() {
    }

    public String getColour() {
        return colour;
    }

    public abstract double surface() ;

    public abstract  double volume() ;

    public void display() {
        System.out.format("colour: %-10s surface: %-10.3f volume: %.3f\n", colour, surface(), volume());
    }
}
