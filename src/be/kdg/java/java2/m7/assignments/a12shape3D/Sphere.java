package be.kdg.java.java2.m7.assignments.a12shape3D;

public class Sphere extends Shape3D {
    private double radius = 1.0;

    public Sphere() {}

    public Sphere(double radius) {
        this.radius = radius;
    }

    public Sphere(String colour, double radius) {
        super(colour);
        this.radius = radius;
    }

    @Override
    public double surface() {
        return 4 * Math.PI * radius * radius;
    }

    @Override
    public double volume() {
        return (4.0 / 3.0) * Math.PI * Math.pow(radius, 3);
    }
}
