package be.kdg.java.java2.m7.assignments.a13student;

public class Student extends Person {
    private int number;

    public Student(String phone, String name, int number) {
        super(phone, name);
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("Student number: %s ", number) + super.toString();
    }
}
