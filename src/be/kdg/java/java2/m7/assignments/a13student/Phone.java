package be.kdg.java.java2.m7.assignments.a13student;

public class Phone {
    protected String number;

    public Phone(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("Number: %s", number);
    }
}
