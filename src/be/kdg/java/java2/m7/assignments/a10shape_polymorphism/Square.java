package be.kdg.java.java2.m7.assignments.a10shape_polymorphism;

public class Square extends Rectangle {
    public Square() {
    }

    public Square(int x, int y) {
        super(x, y);
    }

    public Square(int size) {
        setSize(size);
    }

    public Square(int x, int y, int size) {
        super(x, y, size, size);
    }

    public void setSize(int size) {
        super.width = size;
        super.height = size;
    }
}
