package be.kdg.java.java2.m7.assignments.a10shape_polymorphism;

public class ShapeApp {
    public static void main(String[] args) {
        Shape[] shapes = {new Circle(5), new Rectangle(2,3, 4, 5), new Square(10)};
        for (Shape shape : shapes) {
            System.out.println(shape);
        }
    }
}
