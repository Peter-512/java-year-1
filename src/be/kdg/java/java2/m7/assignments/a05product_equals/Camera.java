package be.kdg.java.java2.m7.assignments.a05product_equals;

import java.util.Objects;

public class Camera extends Product {
    private int pixels;

    public Camera(String code, String description, double price, int pixels) {
        super(code, description, price);
        this.pixels = pixels;
    }

    public int getPixels() {
        return pixels;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Camera camera = (Camera) o;
        return getPixels() == camera.getPixels();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getPixels());
    }

    @Override
    public String toString() {
        return super.toString() + String.format("Camera: %d pixels \n", getPixels());
    }
}
