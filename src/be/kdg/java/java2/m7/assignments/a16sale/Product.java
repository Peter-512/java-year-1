package be.kdg.java.java2.m7.assignments.a16sale;

import java.util.Objects;

public class Product {
	private final int number;
	private final String description;
	private final double price;
	private final int vatPercentage;

	public Product(int number, String description, double price, int vatPercentage) {
		this.number = number;
		this.description = description;
		this.price = price;
		this.vatPercentage = vatPercentage;
	}

	public double getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Product product = (Product) o;
		return number == product.number && Double.compare(product.getPrice(), getPrice()) == 0 && vatPercentage == product.vatPercentage && Objects.equals(getDescription(), product.getDescription());
	}

	@Override
	public int hashCode() {
		return Objects.hash(number, getDescription(), getPrice(), vatPercentage);
	}

	public double getPriceVatInclusive() {
		return getPrice() * (1 + (double) vatPercentage / 100);
	}

	public double getImportTax() {
		return 0;
	}

	@Override
	public String toString() {
		return String.format("%d %s €%.2f %d%%", number, description, price, vatPercentage);
	}
}
