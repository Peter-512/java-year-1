package be.kdg.java.java2.m7.assignments.a16sale;

import java.util.Objects;

public class SalesLine {
	private final Product product;
	private int quantity;

	public SalesLine(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public void show() {
		System.out.printf("%-42squantity:%3d\tprice: €%6.2f\tVAT: €%5.2f\ttotal: €%6.2f\n", product.getDescription(), quantity, getPrice(), getPriceVatInclusive() - getPrice(), getPriceVatInclusive());
	}

	public Product getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void addQuantity(int quantity) {
		this.quantity += quantity;
	}

	public double getVat() {
		return (getPriceVatInclusive() - getPrice());
	}

	public double getPrice() {
		return product.getPrice() * quantity;
	}

	public double getPriceVatInclusive() {
		return product.getPriceVatInclusive() * quantity;
	}

	@Override
	public String toString() {
		return String.format("%dx ", quantity) + product.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SalesLine salesLine = (SalesLine) o;
		return Objects.equals(getProduct(), salesLine.getProduct());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getProduct());
	}
}
