package be.kdg.java.java2.m7.assignments.a16sale;

public class ImportProduct extends Product {
	private final double importTax;

	public ImportProduct(int number, String description, double price, int vatPercentage, double importTax) {
		super(number, description, price, vatPercentage);
		this.importTax = importTax;
	}

	@Override
	public double getImportTax() {
		return importTax;
	}

	@Override
	public String toString() {
		return super.toString() + String.format(" €%.2f", importTax);
	}
}
