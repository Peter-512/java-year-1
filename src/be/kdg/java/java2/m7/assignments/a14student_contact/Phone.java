package be.kdg.java.java2.m7.assignments.a14student_contact;

import java.util.Objects;

public class Phone {
	private String number;
	private String type;
	private final int LENGTH_FIXED = 9;
	private final int LENGTH_MOBILE = 10;

	public Phone(String type, String number) {
		this.number = number;
		this.type = type;
	}

	public boolean isValid() {
		if (!number.startsWith("0")) return false;
		if (type.equals("fixed")) return number.length() == LENGTH_FIXED;
		if (type.equals("mobile")) return number.length() == LENGTH_MOBILE;
		return false;
	}


	public String getNumber() {
		return number;
	}


	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Phone:\n");
		sb.append("number= ").append(number).append('\n');
		sb.append("type= ").append(type).append('\n');
		return sb.toString();
	}
}
