package be.kdg.java.java2.m7.assignments.a14student_contact;

public class Contact {
	private String email;
	private Phone fixed;
	private Phone mobile;

	public Contact(String email, String fixed, String mobile) {
		this.email = email;
		this.fixed = new Phone("fixed", fixed);
		this.mobile = new Phone("mobile", mobile);
	}

	public String getEmail() {
		return email;
	}

	public Phone getFixed() {
		return fixed;
	}

	public Phone getMobile() {
		return mobile;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Contact{");
		sb.append("email='").append(email).append('\'');
		sb.append(", fixed=").append(fixed.getNumber());
		sb.append(", mobile=").append(mobile.getNumber());
		sb.append('}');
		return sb.toString();
	}
}
