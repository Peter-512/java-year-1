package be.kdg.java.java2.m7.assignments.a14student_contact;

public class Person {
private String name;
private Contact contact;

	public Person(String name, String email, String fixed, String mobile) {
		this.name = name;
		this.contact = new Contact(email, fixed, mobile);
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Person:").append("\n");;
		sb.append("name= ").append(name).append("\n");;
		sb.append("email= ").append(contact.getEmail()).append("\n");
		sb.append("fixed phone= ").append(contact.getFixed().getNumber()).append("\n");;
		sb.append("mobile phone= ").append(contact.getMobile().getNumber()).append("\n");;
		return sb.toString();
	}
}
