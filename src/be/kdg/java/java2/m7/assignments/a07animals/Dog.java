package be.kdg.java.java2.m7.assignments.a07animals;

public class Dog extends Animal {

    public Dog(String name, String breed, String color, String CHIP_NUMBER) {
        super(name, breed, color, CHIP_NUMBER);
        super.setTagLine("I'm a lit dog");
    }
}
