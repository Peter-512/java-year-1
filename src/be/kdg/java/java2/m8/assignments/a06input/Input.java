package be.kdg.java.java2.m8.assignments.a06input;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Input {
	public static void main(String[] args) {
		double number = 0;
		boolean isInputOK;
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.print("Please enter a decimal number between 0 and 10: ");
			try {
				number = keyboard.nextDouble();
				isInputOK = true;
				if (number < 0 || number >= 10) {
					System.out.println("That number is not between 0 and 10!");
					isInputOK = false;
				}
			} catch (InputMismatchException e) {
				System.out.println("That's not a decimal number!");
				keyboard.next();
				isInputOK = false;
			}
		} while (!isInputOK);

		DecimalFormat df = new DecimalFormat("#.##########");
		String s = df.format(number);
		System.out.printf("Thanks, %s is a valid number!", number);
	}
}
