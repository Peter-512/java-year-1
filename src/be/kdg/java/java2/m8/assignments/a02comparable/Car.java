package be.kdg.java.java2.m8.assignments.a02comparable;

public class Car implements Printable, Comparable {
	//	private String brand;
	//	private String model;
	//	private String licensePlate;

	protected String brand;
	protected String model;
	protected String licensePlate;

	public Car(String brand, String model, String licensePlate) {
		this.brand = brand;
		this.model = model;
		this.licensePlate = licensePlate;
	}

	@Override
	public boolean isEqualTo(Object o) {
		if (!(o instanceof Car car)) return false;
		if (this == o) return true;
		return brand.equals(car.brand) &&
				model.equals(car.model);
	}

	@Override
	public boolean isGreaterThan(Object o) {
		if (!(o instanceof Car car)) return false;
		if (brand.compareTo(car.brand) > 0) return true;
		if (model.compareTo(car.model) > 0) return true;
		return false;
	}

	@Override
	public boolean isLessThan(Object o) {
		if (!(o instanceof Car car)) return false;
		if (brand.compareTo(car.brand) > 0) return false;
		if (model.compareTo(car.model) > 0) return false;
		return true;
	}

	@Override
	public void print() {
		System.out.println("Car");
		System.out.println("===");
		System.out.printf("Brand: \t\t\t%s %n", brand);
		System.out.printf("Model: \t\t\t%s %n", model);
		System.out.printf("License plate: \t%s %n", licensePlate);

	}
}
