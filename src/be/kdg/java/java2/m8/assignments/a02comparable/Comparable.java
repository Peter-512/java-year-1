package be.kdg.java.java2.m8.assignments.a02comparable;

public interface Comparable {
	boolean isEqualTo(Object o);

	boolean isGreaterThan(Object o);

	boolean isLessThan(Object o);
}
