package be.kdg.java.java2.m8.assignments.a02comparable;

public interface Printable {
	void print();
}
