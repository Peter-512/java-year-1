package be.kdg.java.java2.m8.assignments.a02comparable;

public class Rectangle extends Shape {
	private int width;
	private int height;

	public Rectangle(int x, int y, int width, int height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}

	@Override
	public double getArea() {
		return width * height;
	}

	@Override
	public double getPerimeter() {
		return 2 * width + 2 * height;
	}

	@Override
	public boolean isEqualTo(Object o) {
		if (!(o instanceof Rectangle rect)) return false;
		if (this == o) return true;
		return width == rect.width && height == rect.height;
	}

//	@Override
//	public boolean isGreaterThan(Object o) {
//		if (!(o instanceof Rectangle rect)) return false;
//		return getArea() > rect.getArea();
//	}
@Override
public boolean isGreaterThan(Object o) {
	if (!(o instanceof Rectangle rect)) return false;
	if (width > rect.width) return true;
	if (height > rect.height) return true;
	return false;
}

	@Override
	public boolean isLessThan(Object o) {
		if (!(o instanceof Rectangle rect)) return false;
		return getArea() < rect.getArea();
	}

	@Override
	public void print() {
		System.out.println("Rectangle");
		System.out.println("======");
		System.out.printf("Position: \t(%d, %d) %n", getX(), getY());
		System.out.printf("Width: \t\t%d %n", width);
		System.out.printf("Height: \t%d %n", height);
	}
}
