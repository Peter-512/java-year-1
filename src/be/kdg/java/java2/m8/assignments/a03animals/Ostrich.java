package be.kdg.java.java2.m8.assignments.a03animals;

public class Ostrich implements Named, EggLaying {
	private String name;
	private int numberOfEggsPerYear;

	public Ostrich(String name, int numberOfEggsPerYear) {
		this.name = name;
		this.numberOfEggsPerYear = numberOfEggsPerYear;
	}

	@Override
	public int getNumberOfEggsPerYear() {
		return numberOfEggsPerYear;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return String.format("Name: %s %nEggs: %d %n", getName(), getNumberOfEggsPerYear());
	}
}
