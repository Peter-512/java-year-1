package be.kdg.java.java2.m8.assignments.a03animals;

public class Animals {
	private Named[] animals;
	private int amount = 0;

	public Animals() {
		animals = new Named[100];
	}

	public void add(Named obj) {
		animals[amount++] = obj;
	}

	public void print() {
		for (int i = 0; i < amount; i++) {
			System.out.println(animals[i]);
		}
	}
}
