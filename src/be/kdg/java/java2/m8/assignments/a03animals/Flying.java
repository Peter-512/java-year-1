package be.kdg.java.java2.m8.assignments.a03animals;

public interface Flying {
	int getMaxFlyingSpeed();
}
