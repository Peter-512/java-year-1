package be.kdg.java.java2.m8.assignments.a01interfaces;

public interface Printable {
	void print();
}
