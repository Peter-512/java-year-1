package be.kdg.java.java2.m8.assignments.a01interfaces;

public class Car implements Printable {
	private String brand;
	private String model;
	private String licensePlate;

	public Car(String brand, String model, String licensePlate) {
		this.brand = brand;
		this.model = model;
		this.licensePlate = licensePlate;
	}

	@Override
	public void print() {
		System.out.println("Car");
		System.out.println("===");
		System.out.printf("Brand: \t\t\t%s %n", brand);
		System.out.printf("Model: \t\t\t%s %n", model);
		System.out.printf("License plate: \t%s %n", licensePlate);

	}
}
