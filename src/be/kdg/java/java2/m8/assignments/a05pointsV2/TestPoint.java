package be.kdg.java.java2.m8.assignments.a05pointsV2;

public class TestPoint {
	public static void main(String[] args) {
		Point p1 = new Point(0,0);
		System.out.println(Point.getCOUNT());
		System.out.println(Point.getCOLOR());

		Point p2 = new Point(20,0);
		System.out.println(Point.getCOUNT());
		System.out.println(Point.getCOLOR());

		Point p3 = new Point(0,35);
		System.out.println(Point.getCOUNT());
		System.out.println(Point.getCOLOR());
	}
}
