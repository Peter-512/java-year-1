package be.kdg.java.java2.m8.assignments.a04pointsV1;

public class Point {
	private int x;
	private int y;
	public static final String COLOR = "red";
	public static int COUNT = 0;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
		COUNT++;
	}


}
